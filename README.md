# Monitoring Development Board based on InfluxDB and Grafana

# Monitoring Development Board pada Platform InfluxDB dan Grafana

### ABSTRACT

Purpose: Designing a sensor data monitoring system using a time series database and monitoring platform on a Development
Board device. Design/methodology/approach: It begins with a needs analysis, such as the preparation of the required
software and hardware, followed by the creation of the system architecture that will be adopted. Then the development
process from a predetermined design to the testing process to ensure the dashboard page can display data according to a
predetermined scenario. Findings/result: From the research that has been done, produces a design of sensor data that is
sent using the MQTT protocol via Node-RED, then stored in a time series database (InfluxDB) and displayed on the Grafana
dashboard display. Originality/value/state of the art: Sensor data monitoring dashboard on Development Board devices

### ABSTRAK

Tujuan: Merancang sebuah sistem monitoring data sensor menggunakan time series database dan platform monitoring pada
sebuah perangkat Development Board. Perancangan/metode/pendekatan: Diawali dengan analisis kebutuhan seperti penyiapan
perangkat lunak serta perangkat keras yang dibutuhkan, dilanjutkan pembuatan arsitektur sistem yang akan diadopsi.
Kemudian proses development dari rancangan yang telah ditentukan sampai proses testing untuk memastikan halaman
dashboard bisa menampilkan data sesuai dengan skenario yang telah ditentukan. Hasil: Dari penelitian yang telah
dilakukan menghasilkan sebuah perancangan data sensor yang dikirimkan menggunakan protokol MQTT melalui Node-RED,
selanjutnya disimpan pada sebuah database time series(InfluxDB) dan ditampilkan pada tampilan dashboard Grafana.
Keaslian/ state of the art: Dashboard monitoring data sensor pada perangkat Development Board

### PENDAHULUAN

Sebuah peradaban dunia fisik telah bertransformasi menjadi digital sehingga semua dapat saling terhubung, pertumbuhan
yang sangat pesat smart device dan teknologi mengakibatkan manusia dapat berkomunikasi kapan saja dan dimana saja.
Revolusi Industri 4.0 meningkatkan trend teknologi Internet of Things yang mana berfokus pada interkonektivitas,
otomatisasi, otonomi, machine learning, dan data real time[1]–[5]. Salah satu yang menjadi tantangan pada Internet of
Things adalah proses pengiriman data secara real time dari smart device sebelum dilakukan pengolahan sampai dengan
proses visualiasi data, pengiriman data real time memungkinkan pengambilan keputusan secara real time yang dapat
meningkatkan pendapatan, produktivitas, dan efisiensi. Proses visualisasi menjadi penting karena dapat menggambarkan
sebuah data yang sebelumnya tidak bermakna berubah menjadi sebuah informasi yang mudah untuk dibaca dan difahami, data
visualisasi umumnya berasal dari smart device atau perangkat Internet of Thing yang telah terpasang sensor. Beberapa
peneliti terdahulu telah mengemukakan penelitiannya tentang monitoring data, seperti yang dilakukan oleh Husna dan
Rosyani[6] dalam melakukan monitoring traffic dan log ganggungan pada interface jaringan pada PT Cyber Network
Indonesia, platform yang digunakan dalam melakukan monitoring adalah Zabbix terintegrasi dengan Grafana dan Telegram.
Grafana digunakan untuk memvisualisasikan metrik menjadi grafik-grafik yang menarik serta mudah mengerti. Data secara
real time mengenai penggunaan bandwidth, log problem, dan ketersediaan sumber daya dapat dilihat dimanapun oleh seorang
administrator jaringan. Kemudian ketika terjadi permasalahan akan mendapatkan notifikasi atau pemberitahuan melalui
Telegram. Penelitian yang sama juga memanfaatkan grafana untuk memantau kondisi layanan(service) yang terdapat pada
server, grafana mendapatkan data kondisi service pada server dari Prometheus kemudian untuk mengetahui status dari
service yang dimonitoring menggunakan Telegram[7]. Secara spesifik Prometheus mengumpulkan data resource dalam bentuk
metrik secara langsung ataupun melalui gateway push Prometheus, sebelumnya harus dipasang terlebih dahulu exporter pada
setiap node. Selain digunakan untuk melakukan monitoring pada sebuah jaringan, Grafana digunakan untuk memantau curah
hujan karena dapat memberikan visualisasi yang sangat baik dari hasil yang diperoleh dari sensor curah hujan. Selain
itu, Grafana juga dapat menampilkan keadaan mikrokontroller untuk mendukung kinerja sensor karena mikrokontroller dan
sensor berada di luar ruangan. Informasi tentang keadaan mikrokontroler dan sensor sangat dibutuhkan ketika terjadi
anomali, dapat dengan cepat diidentifikasi dan ditindaklanjuti[8]. Grafana merupakan sebuah tool yang digunakan untuk
monitoring, akan tetapi butuh sebuah sumber data atau data source sebagai masukan dalam menampilkan data yaitu berbagai
macam database. Salah satu yang sering digunakan yaitu InfluxDB. InfluxDB adalah database tidak terstruktur yang
dirancang khusus untuk menyimpan data historis dan dapat memproses data dalam bentuk matriks untuk dianalisis. Karena
masuk ke dalam kategori database tidak terstruktur sehingga sangat berbeda dan mendukung dalam memaksimalkan performa
database khususnya data dalam bentuk time-series[9]. Anih et al[10] menggunakan InfluxDB dalam menyimpan data
time-series berupa suhu harian minimum periode 10 tahun(1981-1990) di kota Melbourne, Australia. Tujuan dari penyimpanan
data tersebut untuk melakukan preprocessing sebelum dilakukan penambangan data, karena data preprocessing merupakan
modal awal untuk dalam penambangan data namun para peneliti mengabaikannya. Data diolah menggunakan bahasa pemrograman
Python dan menguraikan menjadi tren, musiman, dan komponen residu. InfluxDB menyediakan sebuah Application Interface
Programming dengan banyak dukungan bahasa pemrograman dan juga platform Internet of Things seperti Node-RED. Node-RED
merupakan sebuah alat untuk pengambangan yang berbasis JavaScript dibuat menggunakan Node.js, biasanya digunakan dalam
pengembangan Internet of Things dengan menghubungkan API secara bersama, perangkat keras, dan layanan online yang
dikembangkan oleh IBM. Node-RED menyediakan dashboard dengan tampilan sistem berupa kontrol tombol, gauge maupun
grafik[11]–[13]. Meela A et al[14] menggunakan Node-RED untuk memonitor suhu industri menggunakan NodeMCU ESP8266
sebagai pengontrol dan sensor suhu terpasang dengan pengiriman data yang digunakan adalah Message Queue Telemetry
Transport (MQTT). Alasan menggunakan MQTT adalah karena salah satu protokol komunikasi ringan dengan basis internet,
sedangkan penyajian datanya menggunakan Node-RED. Pada penelitian yang dilakukan oleh Duraiswamy R et al[15] menggunakan
Node-RED dalam merancang sebuah sistem pertanian automatis di dalam ruangan berbasis Internet of Things yang
memanfaatkan cloud IBM Bluemix. Parameter yang dipantau dalam berkontribusi pertumbuhan tanaman seperti suhu,
kelembaban, suhu mesin, intensitas cahaya, dan tingkat pH agar tetap berada pada kondisi yang ideal. Ketika terjadi hal
yang tidak ideal, sistem akan memperingatkan kepada pemilik perkebunan agar segera mengambil tindakan yang dibutuhkan.
Pada penelitian ini akan menggabungkan sistem monitoring perangkat Development Board menggunakan Node-RED, InfluxDB,
serta Grafana yang diharapkan mampu untuk memonitoring data sensor secara real-time yang terdapat pada perangkat
Development Board.

### TINJAUAN PUSTAKA

> To do

#### Development Board

Merupakan sebuah board yang dikembangkan oleh Politeknik Negeri Malang, jurusan Teknologi Informasi untuk pembelajaran
Internet of Things khususnya anak-anak SMK/SMA. Board tersebut sudah dibekali sensor dan aktuator seperti DHT11, LDR,
dan hc-sr04. Sedangkan untuk aktuator terdiri dari 9 buah LED, infra merah, Relay, buzzer, serta kipas(FAN).
Microcontroller menggunakan ESP8266 sehingga untuk masalah konektifitas sudah dapat langsung dimanfaatkan seperti
bluetooth dan Wireless, karena sensor dan aktuator telah terpasang sehingga ketika akan menggunakan development board
tersebut sudah tidak perlu dilakukan wiring atau pengkabelan kembali dan bisa fokus untuk membuat program dengan
memanfaatkan sensor atau aktuator yang telah tersedia.

#### InfluxDB

InfluxDB merupakan sebuah database non-struktural open-source yang biasanya digunakan untuk menyimpan data time series,
dikembangkan oleh perusahaan InfluxData. Ditulis menggunakan bahasa pemrograman Go untuk penyimpanan dan menerima data
time series seperti pada bidang monitoring operasional, metrik pada sebuah aplikasi, sensor data IoT, dan untuk
melakukan analisis real-time. InfluxDB juga menyediakan Application Programming Language(API) digunakan dalam
penyimpanan dan query data, mengolahnya di background untuk kebutuhan `Export, Transform, dan Load(ETL)` atau monitoring
serta peringatan, dasbor pengguna, dan memvisualisasikan kemudian explorasi data serta banyak lagi.

#### Node-RED

Node-RED adalah sebuah tool pengembangan berbasis flow untuk pemrograman visual yang sebenarnya dikembangkan oleh IBM
untuk pengkabelan sekaligus perangkat-perangkat keras, API, dan layanan-layanan online sebagai bagian dari Internet of
Things. Nodef-RED menyediakan sebuah web berbasis browser sebagai flow editor, yang dapat menggunakan membuat
fungsi-fungsi JavaScript. Elemen-elemen aplikasi dapat disimpan atau dibagi untuk digunakan kembali. Runtime Node-RED
dibangun di atas Node.js. Flow yang dibuat dalam Node-RED disimpan menggunakan format JSON. Sejak versi 0.14, node MQTT
dapat membuat property dengan konfigurasi koneksi TLS.

#### Grafana

Grafana merupakan sebuah open source analisis multi-platform dan aplikasi web visualisasi interaktif yang menyediakan
grafik, chart, serta notifikasi untuk web ketika terhubung pada data source yang mendukung. Versi Grafana enterprise
yang berlisensi juga tersedia dengan tambahan kemampuan adalah sebuah installasi self-hosted atau sebuah akun pada
layanan cloud Grafana Labs. Selanjutnya juga dapat diperluas melalui sebuah sistem plugin. Seorang pengguna dapat
membuat dashboard monitoring sistem yang kompleks menggunakan interaktif query builder. Grafana dibagi pada sebuah front
end dan back end, ditulis masing-masing menggunakan TypeScript dan Go. Sebagai sebuah tool visualisasi, Grafana terkenal
dengan komponen dalam monitoring stack, sering menggunakan kombinasi dengan database time series seperti InfluxDB,
Prometheus dan Graphite

### METODE PENELITIAN

Ketika melakukan penelitian ini beberapa kebutuhan yang harus dipenuhi untuk perangkat hardware yaitu seperangkat
laptop, sebuah Raspberry Pi 4B 8 GB, dan seperangkat Development Board. Kemudian untuk perangkat lunak yang harus
terpasang yaitu message broker menggunakan Mosquitto, InfluxDB, Node-RED, dan Grafana. Seperangkat laptop digunakan
untuk melakukan development sampai dengan pengujian, Raspberry dijadikan sebuah server untuk melakukan installasi
message broker, database, Node Red, serta Grafana. Selanjutnya untuk Development Board yang telah terpasang sensor dan
aktuator akan mengirimkan datanya ke server, Raspberry.

Setelah semua kebutuhan terpenuhi dan dilakukan konfigurasi atau installasi, sebelum menggunakan perangkat sebenarnya
yaitu Development Board terlebih dahulu melakukan ujicoba message broker, InfluxDB, Node-RED, dan Grafana menggunakan
data dummy atau data yang dihasilkan dengan cara di-generate menggunakan Node-RED. Setelah semua tidak terjadi kendala,
data bisa tersimpan di database influxDB dan Grafana bisa menampilkan data tersebut dengan sebuah dashboard dilanjutkan
dengan data dari sebenarnya yang dihasilkan dari Development Board. Secara umum untuk skema atau rancangan dari alur di
atas dapat disajikan menggunakan gambar di bawah ini

![](images/01.png)

Gambar di atas menunjukkan proses data yang dihasilkan dari Development Board yang mengirimkan data ke server sampai
dengan data tersebut bisa tampil pada sebuah dashboard monitoring. Secara terperinci diawali dengan Development Board
mengirimkan data sensor suhu(DHT11), data sensor cahaya(LDR), dan sensor ultrasonik(HC-SR04) menggunakan koneksi WIFI
melalui protokol Message Queueing Telemetry Transport(MQTT) pada sebuah topic tertentu. Data sensor tersebut yang telah
dikirimkan atau publish akan diterima oleh semua client yang melakukan subcribe, termasuk Node-RED. Selain melakukan
subcribe, Node-RED sekaligus menyimpan datanya pada sebuah database InfluxDB. Setelah datanya tersimpan ke dalam
InfluxDB, kemudian data tersebut dijadikan datasource pada Grafana yang selanjutnya membuat template dashboad sesuai
dengan kebutuhan.

### HASIL DAN PERCOBAAN

Dalam melakukan percobaan pada penelitian ini menggunakan koneksi wireless yang menghubungkan development board dengan
Raspberry sebagai server. Sementara untuk Mosquitto, database(InfluxDB), Node-RED, dan Grafana dilakukan installasi pada
server yang sama yaitu Raspberry untuk memudahkan konfigurasi agar proses pembuatan dashboard monitoring data sensor
dapat dengan mudah dilakukan. Khusus untuk Node-RED tidak perlu dilakukan installasi karena pada Raspberry ketika
melakukan installasi sistem operasi Raspbian sudah terinstall khususnya untuk versi yang dekstop, praktik terbaiknya
adalah untuk Mosquitto sampai dengan Grafana tidak dilakukan dalam mesin yang sama karena masalah keamanan dan pembagian
sumber daya jika dimanfaatkan untuk melakukan pengelolaan data dengan volume yang banyak.

#### Data sensor aktuator Development Board

Development board yang digunakan berbasis microcontroller menggunakan NodeMCU ESP8266 dengan bahasa pemrograman C/C++,
data yang dikirimkan adalah data sensor cahaya, data sensor suhu kelembaban, dan data sensor ultrasonic. Pengiriman data
dilakukan setiap 5 detik menggunaan protokol MQTT, pemilihan penggunaan protokol ini karena dikenal sebagai protokol
yang ringan khususnya digunakan untuk microcontroller dibandingkan misalkan menggunakan protokol http yang harus
terhubung ketika akan mengirimkan data. Data yang dipublish ke message broker menggunakan topik `/smk-data`, topik
adalah sebuah kata kunci yang mana digunakan oleh client lain agar dapat menerima data ketika melakukan subscribe(
berlangganan). Format data yang digunakan ketika dikirimkan menggunakan format JSON dengan cara serialisasi, serialisasi
adalah sebuah proses mengubah data ke dalam sebuah objek untuk disimpan atau dikirimkan melalui media jaringan. Format
data yang dipublish dapat dilihat seperti berikut

```json
{
  "mac": "DC:4F:22:76:35:2A",
  "IP": "192.168.0.110",
  "ldr": 66,
  "src04": 234,
  "temp": 31,
  "hum": 58
}
```

Dari format di atas terlihat terdapat attribut seperti `mac, IP, src04, temp, dan hum`. `mac` digunakan untuk menyimpan
informasi mac address dari perangkat development board, IP adalah informasi tentang ip address dari development board,
sedangkan `ldr` adalah untuk menyimpan informasi data sensor tingkat intensitas cahaya sementara `src04` digunakan untuk
menyimpan informasi data sensor ultrasonik berupa jarak objek yang terdeteksi dalam satuan centimeter (CM), `temp`
digunakan untuk menyimpan informasi temperatur dalam satuan Celcius, dan terakhir yaitu `hum` digunakan untuk menyimpan
informasi humadity atau kelembaban dalam satuan persen (%). Perangkat Development board yang dalam penelitian ini
menggunakan 3 buah seperti ditunjukkan pada gambar di bawah ini

![](images/05.jpg)

#### Struktur data sensor InfluXDB

Seperti yang telah disebutkan bahwa InfluxDB merupakan database tidak struktur sehingga ketika akan menyiapkan
penyimpananpun tidak terlalu banyak dilakukan, setelah dilakukan installasi yang perlu dipersiapkan adalah hanya nama
database yang perlu dibuat secara manual karena nanti dibutuhkan oleh Node-RED ketika akan menyimpan data dan Grafana
ketika akan menambahkan data source untuk menampilkan data sensor pada sebuah dashboard.

Pada InfluxDb tidak menggunakan konsep kolom, tetapi menggunakan istilah tags dan fields. tags diibaratkan sebuah index
kolom jika dianalogikan sebuah database relasional sedangkan fields seperti sebuah kolom, begitu juga dengan tabel jika
pada InfluxDB disebut sebagai measurement. Berikut ini adalah penerapan struktur data sensor ultrasonic

```
> show field keys on monitoring from ultrasonic
name: ultrasonic
fieldKey fieldType
-------- ---------
value    float
> show tag keys on monitoring from ultrasonic
name: ultrasonic
tagKey
------
ip
mac
```

Dari struktur data di atas, bahwa `fieldKey` dengan nama `value` dan menggunakan `fieldType float` adalah untuk
menyimpan nilai sensor data ultrasonic. Kemudian untuk `tagKey` adalah `ip dan mac`, `tagKey` nantinya akan dimanfaatkan
ketika pembuatan dashboard monitoring menggunakan Grafana agar dashboard lebih dinamis. Untuk format data yang disimpan
mirip dengan database struktural seperti ditunjukkan di bawah ini

```
time                ip              mac               value
----                --              ---               -----
1646890021166413493 192.168.0.107 98:CD:AC:25:A1:88 43
1646890021196167680 192.168.0.110 DC:4F:22:76:35:2A 68
1646890023099970507 192.168.0.109 DC:4F:22:76:33:25 48                                                                                                                                                                                                                                                                                                                                                                                                                                5555555555555        
```

Jika dilihat dari stuktur data di atas tidak jauh beda dengan database struktural, yang membedakan adalah terdapat
sebuah fieldKey dengan nama `time`. `FieldKey` tersebut terisi secara otomatis ketika data masuk ke database dan yang
selalu membedakan dengan record atau baris yang lain karena sifatnya yang unique dalam format timestamps.

Fitur penting yang terdapat pada InfluxDB yaitu diperkenankan untuk melakukan konfigurasi data retensi, yaitu sebuah
kebijakan atau rule yang mendeskripsikan seberapa lama data disimpan, berapa banyak copy data yang disimpan dalam sebuah
cluster, dan rentang waktu yang dicover oleh shard group(durasi shard group). Konfigurasi tersebut bisa berbeda pada
setiap database yang dibuat

#### Pembuatan konfigurasi pada Node-RED

Secara mendasar Node-RED berfungsi dalam menyediakan komponen untuk menampilkan data sensor ataupun untuk melakukan
kontrol aktuator. Selain penggunaannya yang sederhana yaitu dengan merangkai sebuah node sehingga terbentuk sebuah alur
atau aliran, Node-RED juga menyediakan banyak sekali komponen yang siap digunakan tanpa harus membuat kode program yang
panjang untuk fungsi tertentu. Sebagai contoh ketika akan publish ataupun subscribe data dari protokol MQTT, disana
sudah tersedia dan yang perlu dilakukan adalah melakukan konfigurasi alamat server broker dan topiknya apa. Hal yang
sama ketika akan mengirimkan atau menyimpan data pada database InfluxDB, dibutuhkan beberapa konfigurasi seperti alamat
server InfluxDB, nama database, serta measurements(tabel). Berikut ini ada rancangan flow untuk menyimpan data ke
database InfluxDB

![](images/02.png)

Dari gambar di atas terdapat beberapa node yaitu `Data Sensor In, msg.payload, Parsing data sensor, dan InfluxDB out`
yang memiliki masing-masing. `Data Sensor In` adalah sebuah node MQTT untuk melakukan subcribe data yang di-publish oleh
development board, `msg.payload` merupakan node debug yang sangat membantu dalam testing atau ujicoba sebuah flow karena
datanya akan ditampilkan pada jendela debugging Node-RED, `Parsing data sensor` merupakan sebuah node fungsi untuk
melakukan parsing data JSON ke dalam format yang dibutuhkan oleh InfluxDB, kemudian yang terakhir yaitu `InfluxDB out`
ialah node yang membantu dalam menyimpan data ke dalam database InfluxDB. Khusus untuk sebuah node function atau dalam
hal ini adalah `Parsing data sensor` kita harus menambahkan sebuah baris perintah menggunakan bahasa pemrograman
javascript sesuai yang kita butuhkan.

#### Design dashboard di Grafana

Agar dapat membuat sebuah dashboard langkah yang perlu dilakukan yaitu menambahkan data source pada Grafana, data source
disini adalah database yang akan diambil datanya kemudian ditampilkan pada sebuah dashboard. Selain mendukung data
source dengan model database time series, jenis database lain juga dapat ditambakan seperti untuk logging menggunakna
Elasticsearch, database struktural(MysQL), serta yang berbasis cloud dicontohkan dengan Google Cloud Monitoring.

Proses pembuatan dashboard dilakukan dengan menambahkan panel serta jenis visualisasi yang kita inginkan, visualisasi
yang ditawarkan bisa berupa `Graph, Gauge, Heatmap, bar gauge`, dan yang lain. Panel ini nantinya yang akan menampilkan
data pada dashboard, pada panel juga terdapat `query inspector` untuk mengambil data pada data source yang sebelumnya
telah dikonfigurasi. Di dalam `query inspector` telah tersedia fungsi seperti agregasi(integral, mean, ataupun median),
juga terdapat transformasi(derivatif, difference, stdev, dan banyak yang lain). Pada gambar bawah ini adalah hasil
dashboard yang masing-masing menampilkan data sensor kelembaban, sensor LDR, dan sensor ultrsonic. Development board
yang digunakan sebanyak 3 buah dengan masing-masing ip adalah `192.168.0.107, 192.168.0.109, dan 192.168.0.110`.
Terlihat bahwa data temperatur yang dihasilkan dari ketiga developent board nilainya rentang 31 - 32 &deg;C yang
mengakibatkan bentuk grafiknya lurus horisontal seperti ditunjukkan pada panel `Sensor Temperatur`. Sementara untuk
grafik pada panel `Sensor Humidity, Sensor LDR, dan Sensor Ultrasonic` cenderung dinamis naik turun.

![](images/04.png)

Dari gambar di atas terlihat bahwa data-data sensor ditampilkan dari semua develoment board, untuk menampilkan beberapa
atau hanya salah satu dapat menggunakan `combo box` pada label `address`. Hal ini bisa dilakukan karena untuk alamat ip
atau mac adalah sebuah tagkey sehingga bisa dijadikan filter pada dashboard di Grafana. Selain itu, data yang
ditampilkan pada dashboard bisa kita filter berdasarkan waktu misalkan 5, 15, 30 menit terakhir atau bahkan jam dan
hari. Kemudian agar dashboard terlihat perjalanan datanya, dapat dilakukan dengan mengubah waktu refresh pada pokok
kanan atas setiap 5 detik sampai dengan refresh per hari.

#### Skenario Pengujian Data

Dalam melakukan pengujian pada penelitian ini, skenario yang dilakukan adalah data yang dihasilkan dari sensor pada
Development Board sampai dengan data bisa ditampilkan pada dasboard Grafana. Tabel berikut ini adalah daftar
masing-masing data tersebut

| No | Perangkat          | Data                                                                                 | Skenario                                                                                                                                    | Hasil    |
|----|--------------------|--------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1  | Developmenar board | {"mac":"DC:4F:22:76:35:2A","IP":"192.168.0.110","ldr":66,"src04":234,"temp":31,"hum":58} | Data yang dihasilkan sensor dipublish ke message broker dengan menampilkan pada debug monitor                                               | Berhasil |
| 2  | Node-RED           | {"mac":"DC:4F:22:76:35:2A","IP":"192.168.0.110","ldr":66,"src04":234,"temp":31,"hum":58} | Data sensor dilakukan subscribe kemudian ditambahkan node debug untuk melihat message payload pada panel debugger                           | Berhasil |
| 3  | InfluxDB           | time=1646890026318540918, ip=192.168.0.110, mac=DC:4F:22:76:35:2A,value=66           | Data sensor dicek pada setiap measurement dengan cara melakukan query                                                                       | Berhasil |
| 4  | Grafana            | time=20122-03-10 12:28:12.500, ip=192.168.0.110, mac=DC:4F:22:76:35:2A,value=66      | Data tampil di masing-masing panel pada halaman dashboard ketika grafik di hover menggunakan mouse dengan melihat waktu data tersebut masuk | Berhasil |

Dari tabel di atas menunjukkan untuk setiap pengujian di masing-masing perangkat baik software atau hardware berhasil
dilakukan, data-data sensor dapat dilihat pada setiap langkah sampai dengan tertampil di halaman dashboard.

### KESIMPULAN DAN DISKUSI

Dari pembahasan di atas, didapatkan kesimpulan bahwa untuk menampilkan data sensor dari development board yang merupakan
perangkat internet of things bisa ditampilkan sesuai dengan informasi yang dibutuhkan dan bisa langsung terlihat ketika
masing-masing data sensor mengalami perubahan. Perpaduan antara InfluxDB, Node-RED, dan Grafana bisa menjadi alternative
atau solusi ketika akan menampilkan data serta melakukan analisis data secara cepat.

Hanya saja pada penelitian ini, InfluxDB, Node-RED, dan Grafana masih diinstall dalam server yang sama yaitu Raspberry
akibatnya ketika diterapkan pada kasus yang yang lebih kompleks akan timbul masalah misalkan komputasi dan pengolahan
data menjadi terhambat karena keterbatasan perangkat Raspberry tersebut. Ke depan, ketiga komponen tersebut bisa
dipasang pada mesin yang berbeda atau bisa juga menggunakan container agar tidak ada ketergantungan antara platform
tersebut. Selain itu, kasus yang diterapkan lebih kompleks lagi misalkan untuk melakukan pemantau sumber daya server
seperti konsumsi RAM, kinerja processor, ketersediaan ruang penyimpanan, dan juga performa jaringan.

### DAFTAR PUSTAKA

[1]    S. Munirathinam, “Industry 4.0: Industrial Internet of Things (IIOT),” Adv. Comput., vol. 117, no. 1, pp.
129–164, Jan. 2020.

[2]    B. Wang, X. Liu, and Y. Zhang, “Internet of Things,” Internet Things BDS Appl., pp. 71–127, 2022.

[3]    P. K. Malik et al., “Industrial Internet of Things and its Applications in Industry 4.0: State of The Art,”
Comput. Commun., vol. 166, pp. 125–139, Jan. 2021.

[4]    B. Diène, J. J. P. C. Rodrigues, O. Diallo, E. H. M. Ndoye, and V. V. Korotaev, “Data management techniques for
Internet of Things,” Mech. Syst. Signal Process., vol. 138, p. 106564, Apr. 2020.

[5]    M. Stoyanova, Y. Nikoloudakis, S. Panagiotakis, E. Pallis, and E. K. Markakis, “A Survey on the Internet of
Things (IoT) Forensics: Challenges, Approaches, and Open Issues,” IEEE Commun. Surv. Tutorials, vol. 22, no. 2, pp.
1191–1221, Apr. 2020.

[6]    M. A. Husna and P. Rosyani, “Implementasi Sistem Monitoring Jaringan dan Server Menggunakan Zabbix yang
Terintegrasi dengan Grafana dan Telegram,” JURIKOM (Jurnal Ris. Komputer), vol. 8, no. 6, pp. 247–255, Dec. 2021.

[7]    D. Rahman, H. Amnur, and I. Rahmayuni, “Monitoring Server dengan Prometheus dan Grafana serta Notifikasi
Telegram,” JITSI J. Ilm. Teknol. Sist. Inf., vol. 1, no. 4, pp. 133–138, Dec. 2020.

[8]    K. T. Widagdo, T. I. Bayu, and Y. A. Susetyo, “Pemodelan Sistem Monitoring Sensor Curah Hujan Menggunakan
Grafana,” Indones. J. Comput. Model., vol. 2, no. 2, pp. 1–8, Dec. 2019.

[9]    H. - and W. Andriyani, “STUDI KOMPARASI MENYIMPAN DAN MENAMPILKAN DATA HISTORI ANTARA DATABASE TERSTRUKTUR
MARIADB DAN DATABASE TIDAK TERSTRUKTUR INFLUXDB,” J. Teknol. TECHNOSCIENTIA, pp. 168–174, Feb. 2020.

[10]    T. J. Anih, C. A. Bede, and C. F. Umeokpala, “Detection of Anomalies in a Time Series Data using InfluxDB and
Python,” Dec. 2020.

[11]    P. MacHeso, T. D. Manda, S. Chisale, N. Dzupire, J. Mlatho, and D. Mukanyiligira, “Design of ESP8266 Smart Home
Using MQTT and Node-RED,” Proc. - Int. Conf. Artif. Intell. Smart Syst. ICAIS 2021, pp. 502–505, Mar. 2021.

[12]    N. Bin Kamarozaman and A. H. Awang, “IOT COVID-19 Portable Health Monitoring System Using Raspberry Pi, Node-Red
and ThingSpeak,” IEEE Symp. Wirel. Technol. Appl. ISWTA, vol. 2021-August, pp. 107–112, 2021.

[13]    S. Bharath and C. Khusi, “IoT Based Smart Traffic System Using MQTT Protocol: Node-Red Framework,” 2021 2nd
Glob. Conf. Adv. Technol. GCAT 2021, Oct. 2021.

[14]    P. S. B. MacHeso, T. D. Manda, A. G. Meela, J. S. Mlatho, G. T. Taulo, and J. C. Phiri, “Industrial Temperature
Monitor Based on NodeMCU ESP8266, MQTT and Node-RED,” Proc. - 2021 3rd Int. Conf. Adv. Comput. Commun. Control
Networking, ICAC3N 2021, pp. 740–743, 2021.

[15]    V. David, H. Ragu, R. K. Duraiswamy, and P. Sasikumar, “IoT based Automated Indoor Agriculture System Using
Node-RED and IBM Bluemix,” Proc. 6th Int. Conf. Inven. Comput. Technol. ICICT 2021, pp. 157–162, Jan. 2021.
